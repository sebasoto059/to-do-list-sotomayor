import React from "react"
import TaskItem from "../TaskItem/TaskItem"

const TaskList = ({tareas, finalizarTarea}) => {
    return(
        <div className="flex flex-col h-full rounded-md border-2 border-lime-700 bg-white opacity-60 overflow-auto">
            {
                tareas.length == 0
                ? <span className="text-lime-600 text-4xl font-medium flex self-center">No posee tareas registradas</span>
                : tareas.map(tarea => (
                    <div className="p-2 w-full">
                        <TaskItem tarea={tarea} key={tarea.id} finalizarTarea={finalizarTarea}/>
                    </div>
                ))
            }
        </div>
    )
}

export default TaskList