import React, { useState } from "react"
import { v4 as uuidv4 } from 'uuid'

const TaskForm = ({agregarTarea}) => {
    const [dialog, SetDialog] = useState(false)

    const closeDialog = () => {
        SetDialog(false)
    }

    const openDialog = () => {
        SetDialog(true)
    }

    const enviar = (form) => {
        form.preventDefault()
        const titulo = form.target.titulo.value
        const descripcion = form.target.descripcion.value
        const tarea = {
            titulo,
            descripcion,
            fecha: new Date().toDateString(),
            id: uuidv4(),
            completada: false
        }
        agregarTarea(tarea)
        closeDialog()
    }

    return(
        <div className="flex h-full justify-center items-center">
            <button className="p-2 bg-green-700 rounded-md text-white font-semibold font-sans" onClick={openDialog}>Agregar tarea</button>
            {dialog
                &&
                <div className="absolute top-0 left-0 w-screen h-screen flex justify-center items-center bg-slate-500 opacity-80">
                    <div className="p-4 bg-white flex flex-col rounded-md w-1/2 h-2/6 opacity-100">
                        <form className="flex h-full flex-col gap-4" onSubmit={enviar}>
                            <div className="flex flex-col h-1/6">
                                <label htmlFor="titulo">Ingrese el titulo de la tarea:</label>
                                <input type="text" id="titulo" name="titulo" placeholder="Ingresar titulo..."/>
                            </div>
                            <div className="flex flex-col h-4/6">
                                <label htmlFor="descripcion">Ingrese la descripcion de la tarea:</label>
                                <textarea className="flex flex-auto" type="text" id="descripcion" name="descripcion" placeholder="Ingresar descripcion..."/>
                            </div>
                            <div className="flex justify-around h-1/6">
                                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-md" onClick={closeDialog}>Cancelar</button>
                                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-md" type="submit">Agregar</button>
                            </div>
                        </form>
                    </div>
                </div>
            }
        </div>
    )
}

export default TaskForm