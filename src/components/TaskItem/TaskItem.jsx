import React, {useState} from "react"
import Plus from "./../../assets/plus.svg"
import Minus from "./../../assets/minus.svg"


const TaskItem = ({tarea, finalizarTarea}) => {
    const [mostrar, setMostrar] = useState(false)

    return(
        <div className="flex w-full flex-col p-2 border border-gray-500">
            <div className="flex flex-row justify-between">
                <h3 className={tarea.completada ? "text-lg font-bold line-through" : "text-lg font-bold"}>{tarea.titulo}</h3>
                <button onClick={() => {setMostrar(!mostrar)}}>
                    {mostrar 
                    ?<img className="h-4" src={Minus} alt="" />
                    :<img className="h-4" src={Plus} alt="" />} 
                </button>
            </div>
            {mostrar
            &&
            <div className="flex flex-1 justify-between">
                <p className={tarea.completada ? "line-through" : ""}>{tarea.descripcion}</p>
                {!tarea.completada
                    &&
                    <button className="px-1 bg-green-700 rounded-md text-white font-semibold font-sans" onClick={() => finalizarTarea(tarea.id)}>Finalizar</button>
                }
            </div>
            }
        </div>
    )
};

export default TaskItem