import React, { useState } from "react"
import "./App.css"
import TaskForm from "./components/TaskForm/TaskForm";
import TaskList from "./components/TaskList/TaskList";

function App() {
  const [tareas, setTareas] = useState([])

  const agregarTarea = (tarea) => {
    setTareas([...tareas, tarea])
  }

  const finalizarTarea = (tareaId) => {
    tareas.map(tarea => {
      if (tarea.id == tareaId) {
        tarea.completada = true;
      }
    })
    setTareas([...tareas])
  }

  return (
    <div className="h-screen flex bg-app flex-col">
      <div className="h-1/6 flex justify-center items-center">
        <h1 className="text-white text-6xl font-bold">LISTA DE TAREAS</h1>
      </div>
      <div className="h-4/6 p-2">
        <TaskList tareas={tareas} finalizarTarea={finalizarTarea}/>
      </div>
      <div className="h-1/6">
        <TaskForm agregarTarea={agregarTarea}/>
      </div>
    </div>
  )
}

export default App
